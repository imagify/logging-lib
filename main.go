package imagifylogging

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"io"
	"os"
)

func NewLogger(isProduction bool) *zap.Logger {
	return NewLoggerTo(isProduction, os.Stdout)
}

func NewLoggerTo(isProduction bool, writer io.Writer) *zap.Logger {
	var config zapcore.EncoderConfig
	if isProduction {
		config = zap.NewProductionEncoderConfig()
	} else {
		config = zap.NewDevelopmentEncoderConfig()
	}
	config.LevelKey = "level"
	config.TimeKey = "time"
	config.CallerKey = "caller"
	config.MessageKey = "message"
	encoder := zapcore.NewJSONEncoder(config)
	zapWriter := zapcore.AddSync(writer)
	core := zapcore.NewTee(zapcore.NewCore(encoder, zapWriter, zapcore.DebugLevel))
	return zap.New(core, zap.AddCaller())
}
